package com.gelios.cryptocurrencyrates;

import android.app.Application;
import android.content.Context;

import dagger.hilt.android.HiltAndroidApp;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;

@HiltAndroidApp
public class MainApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        RxJavaPlugins.setErrorHandler(throwable -> {});
    }
}
