package com.gelios.cryptocurrencyrates.retrofit;

import com.gelios.cryptocurrencyrates.data.pogo.RequestResponse;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiServices {

    String BASE_URL = "https://api.coinranking.com/v2/";
    String API_KEY = "25c8aa40a73c3f520bc0d2b84f8ec40c39a2c82aabe695fb";

    @Headers("x-access-token: " + API_KEY)
    @GET("coins")
    Single<RequestResponse> getCoins(@Query("limit") Integer limit,
                                     @Query("offset") Integer offset,
                                     @Query("orderBy") String orderBy,
                                     @Query("orderDirection") String orderDirection);
}
