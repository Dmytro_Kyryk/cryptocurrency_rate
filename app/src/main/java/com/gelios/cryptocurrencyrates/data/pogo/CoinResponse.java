package com.gelios.cryptocurrencyrates.data.pogo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoinResponse {

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("symbol")
    private String symbol;

    @SerializedName("name")
    private String name;

    @SerializedName("color")
    private String color;

    @SerializedName("iconUrl")
    private String iconUrl;

    @SerializedName("marketCap")
    private String marketCap;

    @SerializedName("price")
    private String price;

    @SerializedName("btcPrice")
    private String btcPrice;

    @SerializedName("listedAt")
    private Integer listedAt;

    @SerializedName("change")
    private String change;

    @SerializedName("rank")
    private Integer rank;

    @SerializedName("sparkline")
    private List<String> sparkline;

    @SerializedName("coinrankingUrl")
    private String coinrankingUrl;

    @SerializedName("24hVolume")
    private String dailyVolume;

    public CoinResponse() {
    }

    public CoinResponse(String uuid, String symbol, String name, String color, String iconUrl, String marketCap, String price, String btcPrice, Integer listedAt, String change, Integer rank, List<String> sparkline, String coinrankingUrl, String dailyVolume) {
        this.uuid = uuid;
        this.symbol = symbol;
        this.name = name;
        this.color = color;
        this.iconUrl = iconUrl;
        this.marketCap = marketCap;
        this.price = price;
        this.btcPrice = btcPrice;
        this.listedAt = listedAt;
        this.change = change;
        this.rank = rank;
        this.sparkline = sparkline;
        this.coinrankingUrl = coinrankingUrl;
        this.dailyVolume = dailyVolume;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBtcPrice() {
        return btcPrice;
    }

    public void setBtcPrice(String btcPrice) {
        this.btcPrice = btcPrice;
    }

    public Integer getListedAt() {
        return listedAt;
    }

    public void setListedAt(Integer listedAt) {
        this.listedAt = listedAt;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public List<String> getSparkline() {
        return sparkline;
    }

    public void setSparkline(List<String> sparkline) {
        this.sparkline = sparkline;
    }

    public String getCoinrankingUrl() {
        return coinrankingUrl;
    }

    public void setCoinrankingUrl(String coinrankingUrl) {
        this.coinrankingUrl = coinrankingUrl;
    }

    public String getDailyVolume() {
        return dailyVolume;
    }

    public void setDailyVolume(String dailyVolume) {
        this.dailyVolume = dailyVolume;
    }
}
