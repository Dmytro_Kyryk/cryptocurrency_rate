package com.gelios.cryptocurrencyrates.data;

public enum OrderByEnum {
    MARKET_CAP,
    PRICE,
    CHANGE
}
