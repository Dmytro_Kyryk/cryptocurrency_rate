package com.gelios.cryptocurrencyrates.data;

import java.util.List;

public class CryptocurrencyItem {

    private String uuid;
    private Integer rank;
    private String iconUrl;
    private String fullName;
    private String shortName;
    private String price;
    private String capitalisation;
    private String change;
    private List<String> sparkline;

    public CryptocurrencyItem(String uuid, Integer rank, String iconUrl, String fullName, String shortName, String price, String capitalisation, String change, List<String> sparkline) {
        this.uuid = uuid;
        this.rank = rank;
        this.iconUrl = iconUrl;
        this.fullName = fullName;
        this.shortName = shortName;
        this.price = price;
        this.capitalisation = capitalisation;
        this.change = change;
        this.sparkline = sparkline;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCapitalisation() {
        return capitalisation;
    }

    public void setCapitalisation(String capitalisation) {
        this.capitalisation = capitalisation;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public List<String> getSparkline() {
        return sparkline;
    }

    public void setSparkline(List<String> sparkline) {
        this.sparkline = sparkline;
    }
}
