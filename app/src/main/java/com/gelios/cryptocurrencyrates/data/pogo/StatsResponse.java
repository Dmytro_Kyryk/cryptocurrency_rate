package com.gelios.cryptocurrencyrates.data.pogo;

import com.google.gson.annotations.SerializedName;

public class StatsResponse {

    @SerializedName("total")
    private Integer total;

    @SerializedName("totalMarkets")
    private Integer totalMarkets;

    @SerializedName("totalExchanges")
    private Integer totalExchanges;

    @SerializedName("totalMarketCap")
    private String totalMarketCap;

    @SerializedName("total24hVolume")
    private String total24hVolume;

    public StatsResponse() {
    }

    public StatsResponse(Integer total, Integer totalMarkets, Integer totalExchanges, String totalMarketCap, String total24hVolume) {
        this.total = total;
        this.totalMarkets = totalMarkets;
        this.totalExchanges = totalExchanges;
        this.totalMarketCap = totalMarketCap;
        this.total24hVolume = total24hVolume;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotalMarkets() {
        return totalMarkets;
    }

    public void setTotalMarkets(Integer totalMarkets) {
        this.totalMarkets = totalMarkets;
    }

    public Integer getTotalExchanges() {
        return totalExchanges;
    }

    public void setTotalExchanges(Integer totalExchanges) {
        this.totalExchanges = totalExchanges;
    }

    public String getTotalMarketCap() {
        return totalMarketCap;
    }

    public void setTotalMarketCap(String totalMarketCap) {
        this.totalMarketCap = totalMarketCap;
    }

    public String getTotal24hVolume() {
        return total24hVolume;
    }

    public void setTotal24hVolume(String total24hVolume) {
        this.total24hVolume = total24hVolume;
    }
}
