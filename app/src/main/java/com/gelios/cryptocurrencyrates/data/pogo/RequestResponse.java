package com.gelios.cryptocurrencyrates.data.pogo;

import com.google.gson.annotations.SerializedName;

public class RequestResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private DataResponse data;

    public RequestResponse() {
    }

    public RequestResponse(String status, DataResponse data) {
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataResponse getData() {
        return data;
    }

    public void setData(DataResponse data) {
        this.data = data;
    }
}
