package com.gelios.cryptocurrencyrates.data.pogo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataResponse {

    @SerializedName("stats")
    private StatsResponse stats;

    @SerializedName("coins")
    private List<CoinResponse> coins;

    public DataResponse() {
    }

    public DataResponse(StatsResponse stats, List<CoinResponse> coins) {
        this.stats = stats;
        this.coins = coins;
    }

    public StatsResponse getStats() {
        return stats;
    }

    public void setStats(StatsResponse stats) {
        this.stats = stats;
    }

    public List<CoinResponse> getCoins() {
        return coins;
    }

    public void setCoins(List<CoinResponse> coins) {
        this.coins = coins;
    }
}
