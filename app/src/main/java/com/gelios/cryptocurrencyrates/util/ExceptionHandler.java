package com.gelios.cryptocurrencyrates.util;

import android.content.Context;

import com.gelios.cryptocurrencyrates.R;

import java.net.UnknownHostException;
import java.util.Objects;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;
import retrofit2.HttpException;

public class ExceptionHandler {

    private final Context context;

    @Inject
    public ExceptionHandler(@ApplicationContext Context context) {
        this.context = context;
    }

    public String handleException(Throwable throwable) {
        if (throwable instanceof HttpException &&
                Objects.requireNonNull(throwable.getMessage()).contains("429")) {
            return context.getString(R.string.try_again_later);
        } else if (throwable instanceof UnknownHostException) {
            return context.getString(R.string.no_internet_connection);
        } else {
            return throwable != null ? throwable.getLocalizedMessage() : "Unknown error";
        }
    }
}
