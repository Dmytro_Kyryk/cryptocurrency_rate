package com.gelios.cryptocurrencyrates.repository;

import androidx.paging.PagingState;
import androidx.paging.rxjava3.RxPagingSource;

import com.gelios.cryptocurrencyrates.data.CryptocurrencyItem;
import com.gelios.cryptocurrencyrates.data.pogo.CoinResponse;
import com.gelios.cryptocurrencyrates.retrofit.ApiServices;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CryptocurrencySource extends RxPagingSource<Integer, CryptocurrencyItem> {

    private static final Integer LIMIT = 20;
    private final ApiServices apiServices;

    private final String orderBy;
    private final String orderDirection;

    public CryptocurrencySource(ApiServices apiServices, String orderBy, String orderDirection) {
        this.apiServices = apiServices;
        this.orderBy = orderBy;
        this.orderDirection = orderDirection;
    }

    @Override
    public @NotNull Single<LoadResult<Integer, CryptocurrencyItem>> loadSingle(@NotNull LoadParams<Integer> loadParams) {
        //If page already exist we get it otherwise we start with 0
        int page = loadParams.getKey() != null ? loadParams.getKey() : 0;
        int offSet = 0;
        //calculate offSet
        if (page != 0) {
            offSet = LIMIT * page;
        }
        return apiServices.getCoins(LIMIT, offSet, orderBy, orderDirection)
                //subscribe on io
                .subscribeOn(Schedulers.io())
                //if errors occurs
                .onErrorResumeNext(error -> {
                    throw error;
                })
                //map result to cryptoCurrency list
                .map(requestResponse -> {
                    List<CryptocurrencyItem> resultList = new LinkedList<>();
                    for (int i = 0; i < requestResponse.getData().getCoins().size(); i++) {
                        CoinResponse coin = requestResponse.getData().getCoins().get(i);
                        resultList.add(new CryptocurrencyItem(
                                coin.getUuid(),
                                coin.getRank(),
                                coin.getIconUrl().replace(".svg", ".png"),
                                coin.getName(),
                                coin.getSymbol(),
                                coin.getPrice(),
                                coin.getMarketCap(),
                                coin.getChange(),
                                coin.getSparkline()
                        ));
                    }
                    return resultList;
                })
                //map to loadResult
                .map(list -> toLoadResult(list, page))
                //return error
                .onErrorReturn(LoadResult.Error::new);
    }

    //method to map Movies to loadResult object
    private LoadResult<Integer, CryptocurrencyItem> toLoadResult(List<CryptocurrencyItem> list, int page) {
        return new LoadResult.Page<>(list, page == 0 ? null : page - 1, page + 1);
    }

    @Override
    public @Nullable Integer getRefreshKey(@NotNull PagingState<Integer, CryptocurrencyItem> pagingState) {
        return null;
    }
}
