package com.gelios.cryptocurrencyrates.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.rxjava3.PagingRx;

import com.gelios.cryptocurrencyrates.data.CryptocurrencyItem;
import com.gelios.cryptocurrencyrates.data.OrderByEnum;
import com.gelios.cryptocurrencyrates.repository.CryptocurrencySource;
import com.gelios.cryptocurrencyrates.retrofit.ApiServices;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.core.Flowable;
import kotlinx.coroutines.CoroutineScope;

@HiltViewModel
public class MainActivityViewModel extends ViewModel {

    public static final String MARKET_CAP = "marketCap";
    public static final String PRICE = "price";
    public static final String CHANGE = "change";

    public static final String DESC = "desc";
    public static final String ASC = "asc";

    private final ApiServices apiServices;
    private Flowable<PagingData<CryptocurrencyItem>> pagingDataFlowable;

    private final Map<String, String> orderSettings = new HashMap<>();

    private final MutableLiveData<String> _orderBy = new MutableLiveData<>(MARKET_CAP);
    public final LiveData<String> orderBy = _orderBy;

    private boolean isRefreshed = true;

    @Inject
    public MainActivityViewModel(@NotNull ApiServices apiServices) {
        this.apiServices = apiServices;
        init();
    }

    private void init() {
        Pager<Integer, CryptocurrencyItem> pager = new Pager<>(
                new PagingConfig(20,
                        20,
                        false,
                        20,
                        20 * 499),
                () -> new CryptocurrencySource(apiServices, orderBy.getValue(), orderSettings.get(orderBy.getValue()))
        );
        //init flowable
        pagingDataFlowable = PagingRx.getFlowable(pager);
        CoroutineScope coroutineScope = ViewModelKt.getViewModelScope(this);
        PagingRx.cachedIn(pagingDataFlowable, coroutineScope);

        //init default sorting value
        orderSettings.put(MARKET_CAP, DESC);
        orderSettings.put(PRICE, DESC);
        orderSettings.put(CHANGE, DESC);
    }

    public void setOrderBy(OrderByEnum orderByEnum) {
        isRefreshed = true;
        switch (orderByEnum) {
            case MARKET_CAP:
                if (Objects.equals(orderBy.getValue(), MARKET_CAP)) {
                    if (Objects.equals(orderSettings.get(MARKET_CAP), DESC)) {
                        orderSettings.put(MARKET_CAP, ASC);
                    } else {
                        orderSettings.put(MARKET_CAP, DESC);
                    }
                }
                _orderBy.setValue(MARKET_CAP);
                break;
            case PRICE:
                if (Objects.equals(orderBy.getValue(), PRICE)) {
                    if (Objects.equals(orderSettings.get(PRICE), DESC)) {
                        orderSettings.put(PRICE, ASC);
                    } else {
                        orderSettings.put(PRICE, DESC);
                    }
                }
                _orderBy.setValue(PRICE);
                break;
            case CHANGE:
                if (Objects.equals(orderBy.getValue(), CHANGE)) {
                    if (Objects.equals(orderSettings.get(CHANGE), DESC)) {
                        orderSettings.put(CHANGE, ASC);
                    } else {
                        orderSettings.put(CHANGE, DESC);
                    }
                }
                _orderBy.setValue(CHANGE);
                break;
            default:
                break;
        }
    }

    public void setRefreshed(boolean refreshed) {
        isRefreshed = refreshed;
    }

    public boolean isRefreshed() {
        return isRefreshed;
    }

    public String getOrderDirection() {
        return orderSettings.get(orderBy.getValue());
    }

    public Flowable<PagingData<CryptocurrencyItem>> getPagingDataFlowable() {
        return pagingDataFlowable;
    }
}
