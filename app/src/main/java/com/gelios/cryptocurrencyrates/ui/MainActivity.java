package com.gelios.cryptocurrencyrates.ui;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.GridLayoutManager;

import com.gelios.cryptocurrencyrates.R;
import com.gelios.cryptocurrencyrates.data.OrderByEnum;
import com.gelios.cryptocurrencyrates.databinding.ActivityMainBinding;
import com.gelios.cryptocurrencyrates.ui.list.CryptocurrencyComparator;
import com.gelios.cryptocurrencyrates.ui.list.CryptocurrencyListAdapter;
import com.gelios.cryptocurrencyrates.ui.list.CryptocurrencyLoadStateAdapter;
import com.gelios.cryptocurrencyrates.util.ExceptionHandler;
import com.gelios.cryptocurrencyrates.viewmodel.MainActivityViewModel;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {

    @Inject
    public ExceptionHandler exceptionHandler;

    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;

    private final CryptocurrencyListAdapter listAdapter =
            new CryptocurrencyListAdapter(new CryptocurrencyComparator());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        binding.setLifecycleOwner(this);
        //setting ViewModel for data binding
        binding.setViewModel(viewModel);

        setContentView(binding.getRoot());

        initViews();

        initObservers();

        initListeners();
    }

    private void initViews() {
        binding.cryptocurrencyList.setLayoutManager(new GridLayoutManager(this, 1));
        binding.cryptocurrencyList.setHasFixedSize(true);
        binding.cryptocurrencyList.setNestedScrollingEnabled(false);
        binding.cryptocurrencyList.setAdapter(listAdapter.withLoadStateFooter(
                new CryptocurrencyLoadStateAdapter(listAdapter, exceptionHandler)));

        //init retry callback
        binding.loadState.retryButton.setOnClickListener(view -> listAdapter.retry());

        listAdapter.addLoadStateListener(combinedLoadStates -> {
            LoadState loadState = combinedLoadStates.getRefresh();

            if (loadState instanceof LoadState.Error) {
                //handle exception
                binding.loadState.errorMsg.setText(
                        exceptionHandler.handleException(((LoadState.Error) loadState).getError().getCause())
                );
            } else if (loadState instanceof LoadState.NotLoading && viewModel.isRefreshed()) {
                //scroll to top after refreshing list, must waiting while list will adjusted
                binding.cryptocurrencyList.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        binding.cryptocurrencyList.getViewTreeObserver().removeOnPreDrawListener(this);
                        binding.cryptocurrencyList.smoothScrollToPosition(0);
                        return false;
                    }
                });
                viewModel.setRefreshed(false);
            }
            //set visibility of widgets based on loadState
            binding.progressBar.setVisibility(loadState instanceof LoadState.Loading
                    ? View.VISIBLE : View.GONE);
            binding.loadState.retryButton.setVisibility(loadState instanceof LoadState.Error
                    ? View.VISIBLE : View.GONE);
            binding.loadState.errorMsg.setVisibility(loadState instanceof LoadState.Error
                    ? View.VISIBLE : View.GONE);
            binding.errorContainer.setVisibility(loadState instanceof LoadState.Error
                    ? View.VISIBLE : View.GONE);
            return null;
        });
    }

    private void initObservers() {
        viewModel.getPagingDataFlowable().subscribe(cryptocurrencyItemPagingData ->
                        listAdapter.submitData(getLifecycle(), cryptocurrencyItemPagingData),
                error -> {
                    //catch exception here to prevent crushing
                });
    }

    private void initListeners() {
        //change order by cryptocurrency
        binding.cryptocurrencyTitle.setOnClickListener(view -> {
            viewModel.setOrderBy(OrderByEnum.MARKET_CAP);
            //check, if icon is visible, so we can animate it
            //visibility is handled in xml
            if (binding.cryptocurrencySortIcon.getVisibility() == View.VISIBLE) {
                handleTitleAnimation(OrderByEnum.MARKET_CAP, viewModel.getOrderDirection());
            }
            listAdapter.refresh();
        });
        //change order by price
        binding.priceTitle.setOnClickListener(view -> {
            viewModel.setOrderBy(OrderByEnum.PRICE);
            if (binding.priceSortIcon.getVisibility() == View.VISIBLE) {
                handleTitleAnimation(OrderByEnum.PRICE, viewModel.getOrderDirection());
            }
            listAdapter.refresh();
        });
        //change order by changes
        binding.changeTitle.setOnClickListener(view -> {
            viewModel.setOrderBy(OrderByEnum.CHANGE);
            if (binding.changeSortIcon.getVisibility() == View.VISIBLE) {
                handleTitleAnimation(OrderByEnum.CHANGE, viewModel.getOrderDirection());
            }
            listAdapter.refresh();
        });
    }

    //change vector drawable for correct animations
    private void handleTitleAnimation(OrderByEnum orderBy, String orderDirection) {
        switch (orderBy) {
            case MARKET_CAP:
                //check what animation we should play
                if (orderDirection.equals(MainActivityViewModel.DESC)) {
                    binding.cryptocurrencySortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_desc_to_asc)
                    );
                } else {
                    binding.cryptocurrencySortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_asc_to_desc)
                    );
                }
                ((AnimatedVectorDrawable) binding.cryptocurrencySortIcon.getDrawable()).start();
                break;
            case PRICE:
                if (orderDirection.equals(MainActivityViewModel.DESC)) {
                    binding.priceSortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_desc_to_asc)
                    );
                } else {
                    binding.priceSortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_asc_to_desc)
                    );
                }
                ((AnimatedVectorDrawable) binding.priceSortIcon.getDrawable()).start();
                break;
            case CHANGE:
                if (orderDirection.equals(MainActivityViewModel.DESC)) {
                    binding.changeSortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_desc_to_asc)
                    );
                } else {
                    binding.changeSortIcon.setImageDrawable(
                            ContextCompat.getDrawable(this, R.drawable.avd_asc_to_desc)
                    );
                }
                ((AnimatedVectorDrawable) binding.changeSortIcon.getDrawable()).start();
                break;
        }
    }
}