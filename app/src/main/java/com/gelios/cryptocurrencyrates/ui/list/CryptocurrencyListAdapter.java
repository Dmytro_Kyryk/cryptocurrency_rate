package com.gelios.cryptocurrencyrates.ui.list;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.gelios.cryptocurrencyrates.R;
import com.gelios.cryptocurrencyrates.data.CryptocurrencyItem;
import com.gelios.cryptocurrencyrates.databinding.ItemCryptocurrencyBinding;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class CryptocurrencyListAdapter extends PagingDataAdapter<CryptocurrencyItem, CryptocurrencyListAdapter.CryptocurrencyItemViewHolder> {

    public CryptocurrencyListAdapter(@NonNull DiffUtil.ItemCallback<CryptocurrencyItem> diffCallback) {
        super(diffCallback);
    }

    @Override
    public int getItemViewType(int position) {
        return position == getItemCount() ? CRYPTOCURRENCY_ITEM : LOADING_ITEM;
    }

    @NonNull
    @Override
    public CryptocurrencyItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CryptocurrencyItemViewHolder(
                ItemCryptocurrencyBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull CryptocurrencyItemViewHolder holder, int position) {
        CryptocurrencyItem item = getItem(position);
        if (item != null) {
            ItemCryptocurrencyBinding binding = holder.binding;

            binding.position.setText(String.valueOf(item.getRank()));

            Glide.with(binding.cryptocurrencyImage)
                    .load(item.getIconUrl())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .circleCrop()
                    .into(binding.cryptocurrencyImage);

            binding.fullName.setText(item.getFullName());
            binding.shortName.setText(item.getShortName());

            binding.price.setText(formatNumber(Double.parseDouble(item.getPrice())));
            if (item.getCapitalisation() != null) {
                binding.capitalisation.setText(formatWithSuffix(Double.parseDouble(item.getCapitalisation())));
            }

            //lineChart customization
            binding.sparkline.getXAxis().setEnabled(false);
            binding.sparkline.getAxisLeft().setEnabled(false);
            binding.sparkline.getAxisRight().setEnabled(false);

            binding.sparkline.getDescription().setEnabled(false);
            binding.sparkline.setScaleEnabled(false);
            binding.sparkline.setViewPortOffsets(0f, 0f, 0f, 0f);
            binding.sparkline.getLegend().setEnabled(false);

            //creating dataSet for chartLine
            List<Entry> data = new LinkedList<>();
            for (int i = 0; i < item.getSparkline().size(); i++) {
                if (item.getSparkline().get(i) != null) {
                    data.add(new Entry(i, Float.parseFloat(item.getSparkline().get(i))));
                }
            }
            LineDataSet dataSet = new LineDataSet(data, "");
            //data set customization
            dataSet.setDrawFilled(true);
            dataSet.setLineWidth(1.5f);
            dataSet.setHighlightEnabled(false);
            dataSet.setDrawCircles(false);
            dataSet.setDrawValues(false);

            //color set
            double change = item.getChange() != null ? Double.parseDouble(item.getChange()) : 0.0;
            if (change == 0) {
                binding.change.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_orange_dark));
                dataSet.setFillDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.gradient_orange));
                dataSet.setColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_orange_dark));
            } else if (change > 0) {
                binding.change.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
                dataSet.setFillDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.gradient_green));
                dataSet.setColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
            } else {
                binding.change.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
                dataSet.setFillDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.gradient_red));
                dataSet.setColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
            }

            binding.change.setText(formatChange(change));
            binding.sparkline.setData(new LineData(dataSet));
        }
    }

    static class CryptocurrencyItemViewHolder extends RecyclerView.ViewHolder {

        private final ItemCryptocurrencyBinding binding;

        public CryptocurrencyItemViewHolder(@NonNull ItemCryptocurrencyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String formatChange(Double number) {
        if (number > 0) {
            return String.format(Locale.getDefault(),
                    "+%.2f%c", number, '%');
        } else {
            return String.format(Locale.getDefault(),
                    "%.2f%c", number, '%');
        }
    }

    private String formatNumber(Double number) {
        if (number < 1.0) {
            return String.format(Locale.getDefault(),
                    "$ %1$,.8f", number);
        } else {
            return String.format(Locale.getDefault(),
                    "$ %1$,.2f", number);
        }
    }

    private String formatWithSuffix(Double number) {
        if (number < 1000)
            return String.format(Locale.getDefault(), "$ %.2f", number);
        int exp = (int) (Math.log(number) / Math.log(1000));
        return String.format(Locale.getDefault(),
                "$ %.2f%c", number / Math.pow(1000, exp), "kMBTPE".charAt(exp - 1));
    }

    public static final int LOADING_ITEM = 0;

    public static final int CRYPTOCURRENCY_ITEM = 1;
}
