package com.gelios.cryptocurrencyrates.ui.list;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.gelios.cryptocurrencyrates.data.CryptocurrencyItem;

import org.jetbrains.annotations.NotNull;

public class CryptocurrencyComparator extends DiffUtil.ItemCallback<CryptocurrencyItem> {
    @Override
    public boolean areItemsTheSame(@NonNull @NotNull CryptocurrencyItem oldItem,
                                   @NonNull @NotNull CryptocurrencyItem newItem) {
        return oldItem.getUuid().equals(newItem.getUuid());
    }

    @Override
    public boolean areContentsTheSame(@NonNull @NotNull CryptocurrencyItem oldItem,
                                      @NonNull @NotNull CryptocurrencyItem newItem) {
        return oldItem.getUuid().equals(newItem.getUuid());
    }
}
