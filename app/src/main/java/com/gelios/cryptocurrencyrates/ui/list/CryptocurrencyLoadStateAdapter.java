package com.gelios.cryptocurrencyrates.ui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.LoadState;
import androidx.paging.LoadStateAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.gelios.cryptocurrencyrates.databinding.ItemLoadStateBinding;
import com.gelios.cryptocurrencyrates.util.ExceptionHandler;

import org.jetbrains.annotations.NotNull;

public class CryptocurrencyLoadStateAdapter extends LoadStateAdapter<CryptocurrencyLoadStateAdapter.CryptocurrencyLoadStateViewHolder> {

    private final CryptocurrencyListAdapter adapter;
    private final ExceptionHandler exceptionHandler;

    public CryptocurrencyLoadStateAdapter(CryptocurrencyListAdapter adapter, ExceptionHandler exceptionHandler) {
        this.adapter = adapter;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public @NotNull CryptocurrencyLoadStateAdapter.CryptocurrencyLoadStateViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup,
                                                                                                        @NotNull LoadState loadState) {
        return new CryptocurrencyLoadStateViewHolder(
                ItemLoadStateBinding.inflate(LayoutInflater.from(viewGroup.getContext()))
        );
    }

    @Override
    public void onBindViewHolder(@NotNull CryptocurrencyLoadStateAdapter.CryptocurrencyLoadStateViewHolder holder,
                                 @NotNull LoadState loadState) {
        ItemLoadStateBinding binding = holder.binding;

        if (loadState instanceof LoadState.Error) {
            //get the error
            LoadState.Error loadStateError = (LoadState.Error) loadState;
            //set text of error message
            binding.errorMsg.setText(exceptionHandler.handleException(loadStateError.getError().getCause()));
            //set retry callback
            binding.retryButton.setOnClickListener(v -> adapter.retry());
        }
        //set visibility of widgets based on loadState
        binding.progressBar.setVisibility(loadState instanceof LoadState.Loading
                ? View.VISIBLE : View.GONE);
        binding.retryButton.setVisibility(loadState instanceof LoadState.Error
                ? View.VISIBLE : View.GONE);
        binding.errorMsg.setVisibility(loadState instanceof LoadState.Error
                ? View.VISIBLE : View.GONE);
    }

    static class CryptocurrencyLoadStateViewHolder extends RecyclerView.ViewHolder {

        private final ItemLoadStateBinding binding;

        public CryptocurrencyLoadStateViewHolder(@NonNull ItemLoadStateBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
