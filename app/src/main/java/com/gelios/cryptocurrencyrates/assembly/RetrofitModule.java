package com.gelios.cryptocurrencyrates.assembly;

import com.gelios.cryptocurrencyrates.retrofit.ApiServices;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.scopes.ViewModelScoped;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ViewModelComponent.class)
public class RetrofitModule {

    @ViewModelScoped
    @Provides
    public static ApiServices provideApiServices() {
        return new Retrofit.Builder()
                .baseUrl(ApiServices.BASE_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build()
                .create(ApiServices.class);
    }
}
